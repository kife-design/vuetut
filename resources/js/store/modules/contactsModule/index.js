import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    contacts: {
        data: {}
    },
    filters: {}
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};