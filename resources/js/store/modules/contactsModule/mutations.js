import Vue from 'Vue';

const mutations = {
    CONTACTS_FETCHED(state, contacts) {
        state.contacts = contacts;
    },
    SET_FILTER(state, filter) {
        if (filter.value == "") {
            Vue.delete(state.filters, filter.column);
        } else {
            Vue.set(state.filters, filter.column, filter.value);
        }
    }
};

export default mutations;