<?php

namespace App\Http\Controllers;

use App\Http\Resources\Contact as ContactResource;
use App\Models\Company;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function index(Request $request)
    {
        $contacts = Contact::with('company')->paginate($request->get('limit', 10));

        return ContactResource::collection($contacts);
    }

    public function getDependencies()
    {
        return [
            'companies' => Company::all()->map(function ($company) {
                return [
                    'id'   => $company->id,
                    'name' => $company->name
                ];
            })
        ];
    }

    public function store(Request $request)
    {
        $contact = Contact::create($request->all());

        return new ContactResource($contact);
    }

    public function update(Request $request, Contact $contact)
    {
        $contact->update($request->all());

        return new ContactResource($contact);
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();
    }
}
